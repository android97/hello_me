package buu.mongkhol.hellome

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    val LOG = "MAIN_ACTIVITY"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val edtName = findViewById<EditText>(R.id.edtName)
        val btnHello = findViewById<Button>(R.id.btnHello)
        val txtHello = findViewById<TextView>(R.id.btnHello)
//        btnHello.setOnClickListener(this)
//        btnHello.setOnClickListener(object: View.OnClickListener {
//            override fun onClick(view: View?) {
//                Toast.makeText( this@MainActivity, "Click Me", Toast.LENGTH_LONG).show()
//            }
//
//        })
        btnHello.setOnClickListener {
            Toast.makeText( this@MainActivity, "Hello" + edtName.text.toString(), Toast.LENGTH_LONG).show()
            Log.d(LOG, "Click Me")
            txtHello.text = "Hello " + edtName.text.toString()
        }
    }

    override fun onClick(p0: View?) {
        Toast.makeText( this, "Click Me", Toast.LENGTH_LONG).show()
    }
}